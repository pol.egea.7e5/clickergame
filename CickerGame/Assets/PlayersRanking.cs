using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class PlayersRanking : MonoBehaviour
{
    public string url = "http://localhost:3000/players/ranking";
    public TMPro.TMP_Text[] playerNameTexts;

    [System.Serializable]
    public class PlayerData
    {
        public string name;
        public int score;
    }

    [System.Serializable]
    public class RankingData
    {
        public List<PlayerData> players;
    }

    IEnumerator Start()
    {
        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            RankingData rankingData = JsonUtility.FromJson<RankingData>(www.downloadHandler.text);

            // Process ranking data and display in Unity UI
            for (int i = 0; i < rankingData.players.Count && i < 8; i++)
            {
                playerNameTexts[i].text = rankingData.players[i].name + ": "+ rankingData.players[i].score.ToString();
            }
        }
    }
}