using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickController : MonoBehaviour
{
    public GameObject Score;
    public GameObject Messi;
    public int scorepoint;
    private Text text;
    public float DurationBonus = 1f;
    public int PuntosAdd = 1;
    Animator score;
    Animator messi;
    AudioSource Yandel150;
    public void ClickerPlus()
    {
        messi = Messi.GetComponent<Animator>();
        Yandel150 = Messi.GetComponent<AudioSource>();

        scorepoint +=PuntosAdd;
        messi.speed = scorepoint * 0.05f;
            text = Score.GetComponent<Text>();
            text.text = "SCORE: " + scorepoint;

            score = text.GetComponent<Animator>();
            score.SetTrigger("ScoreAnimation");
       
    }

    public void ActivateBonus()
    {
       StartCoroutine(BonusCoroutine());     
    }

    IEnumerator BonusCoroutine()
    {
        Yandel150 = Messi.GetComponent<AudioSource>();
        Yandel150.pitch = 1.5f;
        Debug.Log("Clicked");
        PuntosAdd = 2;
        yield return new WaitForSeconds(DurationBonus);
        PuntosAdd = 1;
        Yandel150.pitch = 1;
    }


}
