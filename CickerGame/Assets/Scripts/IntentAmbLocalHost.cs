using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class IntentAmbLocalHost : MonoBehaviour
{
    public TMPro.TMP_InputField usernameInput;
    public TMPro.TMP_InputField passwordInput;
    public TMPro.TMP_Text messageText;
    
    public void OnRegisterButtonClicked()
    {
        StartCoroutine(Register(usernameInput.text, passwordInput.text));
    }
    public void OnLoginButtonClicked()
    {
        StartCoroutine(Login(usernameInput.text, passwordInput.text));
    }
    IEnumerator Register(string name, string password)
    {
        string url = "http://localhost:3000/players/register";
        var request = new UnityWebRequest(url, "POST");
        string jsonRequestBody = "{\"name\":\"" + name + "\",\"password\":\"" + password + "\"}";
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonRequestBody);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");

        yield return request.SendWebRequest();
        string responseBody = Encoding.UTF8.GetString(request.downloadHandler.data);
        if (responseBody == "{\"error\":\"El jugador "+name+" ja existeix.\"}")
        {
            Debug.LogError("Error sending request: " + request.error);
        }
        else
        {
            Debug.Log("Register successful!");
            PlayerPrefs.SetString("name", name);
            SceneManager.LoadScene(2);
            Debug.Log("Response: " + responseBody);
        }
    }
    IEnumerator Login(string name, string password)
    {
        string url = "http://localhost:3000/players/login";
        var request = new UnityWebRequest(url, "POST");
        string jsonRequestBody = "{\"name\":\"" + name + "\",\"password\":\"" + password + "\"}";
        byte[] bodyRaw = Encoding.UTF8.GetBytes(jsonRequestBody);
        request.uploadHandler = new UploadHandlerRaw(bodyRaw);
        request.downloadHandler = new DownloadHandlerBuffer();
        request.SetRequestHeader("Content-Type", "application/json");
        
        yield return request.SendWebRequest();
        string responseBody = Encoding.UTF8.GetString(request.downloadHandler.data);
        if (responseBody == "Hi ha un error")
        {
            Debug.LogError("Error sending request: " + request.error);
        }
        else
        {
            Debug.Log("Login successful!");
            PlayerPrefs.SetString("name", name);
            SceneManager.LoadScene(2);
            Debug.Log("Response: " + responseBody);
        }
    }
}
