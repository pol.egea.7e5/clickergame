using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public void LoadMenu()
    {
        SceneManager.LoadScene(2);
    }
    public void LoadRanking()
    {
        SceneManager.LoadScene(3);
    }
    public void LoadGameScene()
    {
        SceneManager.LoadScene(1);
    }
    public void DoExitGame()
    {
        Application.Quit();
    }
}
