using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiController : MonoBehaviour
{
    public void LoadGame()
    {
        SceneManager.LoadScene("ClickerGame");
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
