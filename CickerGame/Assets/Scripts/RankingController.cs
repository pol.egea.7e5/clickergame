using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEngine.Networking;
using System.Net;

public class RankingController : MonoBehaviour
{
    [SerializeField]
    public List<GameObject> positions;
    // Start is called before the first frame update
    void Start()
    {
        RankingPlayers();
    }

    public void RankingPlayers()
    {
        int index = 0;

        // Make a GET request to the API and get the response as a string
        string url = "http://localhost:3000/players/ranking";
        WebClient client = new WebClient();
        string response = client.DownloadString(url);

        // Convert the response string to a list of User objects
        List<User> users = JsonConvert.DeserializeObject<List<User>>(response);

        // Sort the list of users
        //users.Sort();

        foreach (GameObject pos in positions)
        {
            if (index < users.Count) pos.gameObject.GetComponent<TMPro.TMP_Text>().text = users[index].name + ": " + users[index].score;
            else
            {
                pos.gameObject.GetComponent<TMPro.TMP_Text>().text = "";
            }
            index++;
        }
    }
    [System.Serializable]
    public class User:IComparer<User> 
    {
        public string name;
        public int score;

        public User(string name, string score)
        {
            this.name = name;
            this.score = System.Convert.ToInt32(score);
        }

        public int Compare(User x, User y)
        {
            if (x.score >= y.score) return 1;
            return 0;
        }
    }
    


}
