using System;
using System.Net;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static CW.Common.CwInputManager;
using UnityEngine.Networking;

public class UpdatePlayer : MonoBehaviour
{
    public GameObject click;
    private String name2;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void UpdatePlayerScore()
    {

        name2 = PlayerPrefs.GetString("name");
        StartCoroutine(UpdateScore(name2, click.gameObject.GetComponent<ClickController>().scorepoint));
        Debug.Log(name2 + ": "+ click.gameObject.GetComponent<ClickController>().scorepoint);
    }
    IEnumerator UpdateScore(string playerName, int newScore)
    {
        string url = "http://localhost:3000/players/newScore/" + playerName + "/score";

        // Create the JSON payload
        string json = "{\"score\":" + newScore.ToString() + "}";

        // Create the HTTP request
        UnityWebRequest request = UnityWebRequest.Put(url, json);
        request.SetRequestHeader("Content-Type", "application/json");

        // Send the request and wait for the response
        yield return request.SendWebRequest();

        if (request.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError("Error updating player score: " + request.error);
            yield break;
        }

        // Parse the response JSON
        string responseJson = request.downloadHandler.text;
        PlayerData playerData = JsonUtility.FromJson<PlayerData>(responseJson);

        // Log the message
        Debug.Log(playerData.message);
    }
    [Serializable]
    public class PlayerData
    {
        public string name;
        public int score;
        public string message;
    }

    [Serializable]
    public class Response
    {
        public string message;
    }

}
